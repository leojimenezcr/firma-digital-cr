#!/bin/bash

# Firmador
# https://firmador.app
# Tomado de https://git.ucr.ac.cr/LEONARDO.JIMENEZ/ubuntu-install-scripts/-/blob/master/firmador/firmador-install.sh
# Author @leojimenezcr https://gitlab.com/leojimenezcr
#
echo "Se configurará el programa Firmador (firmador.app) para firma digital de Costa Rica en documentos PDF."

echo "Copiando el programa a /opt"
sudo mkdir -p /opt/firmador/
sudo wget -c https://firmador.app/firmador.jar -P /opt/firmador/
sudo wget -c https://firmador.app/firmador.svg -P /opt/firmador/

echo "Creando una entrada del menú."
sudo sh -c 'echo "[Desktop Entry]
Name=Firmador
Comment=Firmar digitalmente archivos PDF
Exec=java -jar /opt/firmador/firmador.jar
Icon=/opt/firmador/firmador.svg
Terminal=false
Type=Application
Categories=Application;Office;
StartupNotify=true
Keywords=firma;pdf;" > /usr/share/applications/firmador.desktop'

echo ""
echo "Se ha configurado Firmador. Antes de ejecutarlo asegúrese de tener Java y el componente de Firma Digital (soportefirmadigital.com) configurados."
