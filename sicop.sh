#! /bin/bash

. /etc/os-release
SDFPATH=""
JSPPATH=""
BASEDIR="$(dirname $0)"
TMPPATH="/tmp/firma-digital"
INSPATH="/opt/sicop"
BINWGET="$( which wget )"
BINGREP="$( which grep )"
BINTR="$( which tr )"
OSNAME=""
OSRELEASE=""

echoerr() { echo "$@" 1>&2; }
usage() {
    echo -e "Uso: $0 [ -i ] [ -f INSTALADOR ]\n" \
        "  -d \t\t\tIntenta descargar el instalador desde el sitio. " \
         "Comportamiento por defecto, si se especifica -d se ignorará -f.\n" \
        "  -f INSTALADOR \tpath al instalador del componente de firma" \
         "para MacOS X, disponible en http://www.componentefirmacr.go.cr.\n" \
        "  -h \t\t\tMuestra esta ayuda.\n" \
        "  -i \t\t\tInstalar dependencias faltantes.\n" 1>&2 
}

while getopts "df:hi" options; do
    case "${options}" in
        d)
            DONWLOADINSTALLER=0
            ;;
        f)
            SDFPATH="${OPTARG}"
            if [ ! -f "$SDFPATH" ] || [ -z "$(file $SDFPATH | grep -i 'xar archive')"  ]; then
                echoerr "Debe especificar el path al instalador del componente de firma" \
                    "para MacOS X, disponible en http://www.componentefirmacr.go.cr."
                usage
                exit 1
            fi
            ;;
        h)
            usage
            exit 1
            ;;
        i)
            INSTALLDEPS=0
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done

echo
echo "Configurador del Componente de Firma para SICOP"
echo "  Basado en las guías de https://fran.cr/"
echo 
echo "Sistema detectado: $PRETTY_NAME"
[ -z "$DONWLOADINSTALLER" ] && [ -n "$SDFPATH" ] && \
    echo "Instalador: $SDFPATH"
[ -n "$DONWLOADINSTALLER" ] || [ -z "$SDFPATH" ] && \
    echo "Se intentará descargar el instalador desde Internet."
echo

mkdir -p "${TMPPATH}"
if [ -n "$DONWLOADINSTALLER" ] || [ -z "$SDFPATH" ]; then
    echo "Descargando instalador desde Internet."
    JSPPATH="$TMPPATH/controladores.jsp"
    $BINWGET -O "$JSPPATH" 'http://www.componentefirmacr.go.cr/componenteFirmaCR/controladores.jsp' &> /dev/null
    SEQ=`$BINGREP -E -m 1 -o "'([0-9]+)'" $JSPPATH | $BINTR -d "''"`
    SDFPATH="$TMPPATH/Componente_Firma.pkg"
    $BINWGET -O "$SDFPATH" 'http://www.componentefirmacr.go.cr/componenteFirmaCR/DownloadFile?hidden_seq='$SEQ
    [[ $? -gt 0 ]] && echoerr "Error al descargar instalador desde Internet." && exit 1
fi

if [ -n "$INSTALLDEPS" ]; then
    echo "Instalando paquetes requeridos: cpio, p7zip."
    sudo apt -qq update && sudo apt -y -qq install cpio p7zip-full
    [[ $? -gt 0 ]] && echoerr "Error al instalar paquetes requeridos." && exit 1
fi

BIN7Z="$( which 7z )"
BINCPIO="$( which cpio )"
if [ -z "$BIN7Z" ] || [ -z "$BINCPIO" ]; then
    echoerr "Se requiere de 7-Zip y cpio instalado."
    exit 1
fi

echo "Creando directorio ~/Documents, requerido por el componente."
mkdir -pv ~/Documents
[[ $? -gt 0 ]] && echoerr "Error al crear directorio 'Documents'" && exit 1

echo "Desempaquetando componente Java desde instalador de MacOS X."
( cd "$TMPPATH" && \
  $BIN7Z x -so $SDFPATH | cpio -i && \
  sudo mkdir -p $INSPATH && \
  sudo cp ${TMPPATH}/Componente_Firma.app/Contents/Java/MerlinkSignMV.jar $INSPATH
)
[[ $? -gt 0 ]] && echoerr "Error al desempaquetar componente Java" && exit 1

sudo cp -v "$BASEDIR/files/sicop.png" $INSPATH && \
echo "Generando Launcher en menú de aplicaciones"
sudo sh -c 'echo "[Desktop Entry]
Name=SICOP (Componente de firma)
Comment=Sistema Integrado de Compras Públicas
Exec=java -jar '"$INSPATH"'/MerlinkSignMV.jar
Icon='"$INSPATH"'/sicop.png
Terminal=false
Type=Application
Categories=Application;Office;
StartupNotify=true
Keywords=firma;sicop;" > /usr/share/applications/sicop.desktop'
[[ $? -gt 0 ]] && echoerr "Error al crear lanzador en el menú de aplicaciones" && exit 1


echo "Copiando script para iniciar componente manualmente."
sudo cp -v $BASEDIR/files/iniciar-sicop.sh $INSPATH && \
sudo chmod +x "$INSPATH/iniciar-sicop.sh"
[[ $? -gt 0 ]] && echoerr "Error al copiar script para iniciar componente" && exit 1

echo "Agregando Workaround para soporte del componente en Firefox"
sudo sh -c 'echo "127.0.0.1   service.componentefirmacr.go.cr" >> /etc/hosts'

echo
echo "Listo. El proceso ha finalizado."
echo "El componente de firma para SICOP quedó instalado en $INSPATH."
echo "Puede ejecutar el componente buscandolo en la lista de aplicaciones por SICOP."
echo "O al dar doble clic en el aplicativo MerlinkSignMV.jar."
echo "En caso de que no funcione, puede utilizar el script 'iniciar-sicop.sh' ubicando en $INSPATH."
#read

exit 0
